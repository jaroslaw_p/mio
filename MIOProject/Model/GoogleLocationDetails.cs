﻿namespace MIOProject.Model
{
	public class GoogleLocationDetails
	{
		public string Name { get; set; }
		public double Lat { get; set; }
		public double Lng { get; set; }
	}
}
