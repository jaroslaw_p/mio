﻿using MIOProject.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIOProject
{
	public class LocationLoader
	{
		public List<GoogleLocationDetails> LoadLocation()
		{
			var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "location");
			var files = Directory.GetFiles(path, "*.json");
			var locations = new List<GoogleLocationDetails>(100);

			foreach (var f in files)
			{
				var locationsFromFile = new GoogleLocations(f).Load()
					.ToList();
				locations.AddRange(locationsFromFile);
			}

			return locations;
		}
	}
}
