﻿using Nest;
using System;

namespace MIOProject
{
	public class ElasticClientFactory
	{
		public static string IndexName => "DailyHourConsumptionOfABilingUnit";

		public static ElasticClient CreateElasticClient()
		{
			var uri = new Uri("http://localhost:9200");
			var settings = new ConnectionSettings(uri);
			settings.DefaultIndex(IndexName);
			settings.EnableDebugMode();
			var elasticClient =
				new ElasticClient(settings);

			return elasticClient;
		}

		public static ElasticClient CreateCustomElasticClient(string indexName)
		{
			var uri = new Uri("http://localhost:9200");
			var settings = new ConnectionSettings(uri);
			settings.DefaultIndex(indexName);
			settings.EnableDebugMode();
			var elasticClient =
				new ElasticClient(settings);

			return elasticClient;
		}
	}
}
