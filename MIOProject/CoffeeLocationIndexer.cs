﻿using MIOProject.Model;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIOProject
{
	public class CoffeeLocationIndexer
	{
		private readonly IElasticClient _elasticClient;

		public CoffeeLocationIndexer(IElasticClient client)
		{
			_elasticClient = client;
		}

		public void Index(List<GoogleLocationDetails> locations)
		{
			//Create index
			var createIndexResponse = _elasticClient.CreateIndex(_elasticClient.ConnectionSettings.DefaultIndex,
				i => i.Mappings(m => m.Map<CoffeeLocation>(mm => mm
					.AutoMap()
					.Properties(p => p.GeoPoint(gp => gp.Name(n => n.Location))))));

			//Index documents
			for (var i = 0; i < locations.Count; i++)
			{
				var item = locations[i];
				_elasticClient.IndexDocument(new CoffeeLocation
				{
					Id = i,
					Name = item.Name,
					Location = GeoLocation.TryCreate(item.Lat, item.Lng)
				});
			}
		}
	}
}
