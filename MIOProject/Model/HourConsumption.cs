﻿using System;

namespace MIOProject.Model
{
	public class HourConsumption
	{
		public DateTime DataOdczytu { get; set; }
		public int StatusZuzycia { get; set; }
		public int Godzina { get; set; }
		public int Zuzycie { get; set; }
		public int? ZuzycieM3 { get; set; }
		public int? PlikzrodlowyPozycjaId { get; set; }
		public int? ZuzycieProfiloweId { get; set; }
		public int RodzajOdczytu { get; set; }
		public DateTime UpdDate { get; set; }
	}
}