﻿using Nest;

namespace MIOProject
{
	class Program
	{
		static void Main(string[] args)
		{
			GeoLocation OurLocation = new GeoLocation(50.262950, 19.013930);

			var coffeeSearche = new CoffeeSearcher();
			var nearMe = coffeeSearche
				.NearMe(OurLocation.Latitude, OurLocation.Longitude);
			nearMe.Wait();
		}

		private static void DeleteIndex(ElasticClient elasticClient)
		{

		}
	}
}
