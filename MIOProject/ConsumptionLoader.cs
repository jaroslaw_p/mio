﻿using Ionic.Zip;
using MIOProject.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MIOProject
{
	public class ConsumptionLoader
	{
		public IEnumerable<Consumption> LoadConsumption(string filePath)
		{
			Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

			using (var zipFile = ZipFile.Read(filePath))
			{
				var zipEntry = zipFile["eksport.csv"];

				using (var s = new StreamReader(zipEntry.OpenReader()))
				{
					while (!s.EndOfStream)
					{
						var readLine = s.ReadLine();
						if (String.IsNullOrEmpty(readLine))
						{
							continue;
						}

						var strings = readLine.Split(';');
						var hourConsumption = strings[12].Replace("\"\"", "'")
							.Replace("\"", "");
						yield return new Consumption
						{
							Id = Convert.ToInt32(strings[0]),
							PunktPoboruNaglowekId = Convert.ToInt32(strings[1]),
							FppId = Convert.ToInt32(strings[2]),
							DzienId = Convert.ToInt32(strings[3]),
							TaryfaId = Convert.ToInt32(strings[4]),
							StrefaId = Convert.ToInt32(strings[5]),
							Kierunek = Convert.ToInt32(strings[6]),
							RodzajOdczytu = Convert.ToInt32(strings[7]),
							Zuzycie = Convert.ToInt32(strings[8]),
							CzyOdsprzedaz = Convert.ToInt32(strings[9]),
							ZuzycieM3 = TryParseNullableInt(strings[10]),
							CzyKompletZuzyc = Convert.ToInt32(strings[11]),
							Godziny = JsonConvert.DeserializeObject<List<HourConsumption>>(hourConsumption)
						};
					}
				}
			}
		}

		public static int? TryParseNullableInt(string s)
		{
			if (String.IsNullOrEmpty(s))
			{
				return null;
			}

			int i;
			if (Int32.TryParse(s, out i))
			{
				return i;
			}

			return null;
		}
	}
}
