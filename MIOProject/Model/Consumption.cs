﻿using System.Collections.Generic;

namespace MIOProject.Model
{
	public class Consumption
	{
		public int Id { get; set; }
		public int PunktPoboruNaglowekId { get; set; }
		public int FppId { get; set; }
		public int DzienId { get; set; }
		public int TaryfaId { get; set; }
		public int StrefaId { get; set; }
		public int Kierunek { get; set; }
		public int RodzajOdczytu { get; set; }
		public int Zuzycie { get; set; }
		public int CzyOdsprzedaz { get; set; }
		public int? ZuzycieM3 { get; set; }
		public int CzyKompletZuzyc { get; set; }
		public List<HourConsumption> Godziny { get; set; }
	}
}
