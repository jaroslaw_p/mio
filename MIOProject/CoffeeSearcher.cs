﻿using MIOProject.Model;
using Nest;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MIOProject
{
	public class CoffeeSearcher
	{
		private readonly ElasticClient _elasticClient;

		public CoffeeSearcher()
		{
			_elasticClient = ElasticClientFactory.CreateCustomElasticClient(IndexNames.MioProject);
		}

		public async Task<CoffeeLocation> NearMe(double lat, double lng)
		{
			try
			{

				var searchResponse = await _elasticClient.SearchAsync<CoffeeLocation>(s => s
					.Query(q => q.Bool(b => b
						.Must(m => m.MatchAll())
						.Filter(f => f.GeoDistance(gd => gd
							.Location(lat, lng)
							.Field(field => field.Location)))))
					.Sort(sort => sort
						.GeoDistance(gd => gd
							.Field(f => f.Location)
							.Points(GeoLocation.TryCreate(lat, lng))
							.Ascending()))
				);

				var nearMe = searchResponse.Documents.FirstOrDefault();

				return nearMe;
			}
			catch (Exception e)
			{
				var message = e.Message;
				throw;
			}
		}
	}
}
