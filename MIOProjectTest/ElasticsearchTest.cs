﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MIOProject;
using System.Threading.Tasks;

namespace MIOProjectTest
{
	[TestClass]
	public class ElasticsearchTest
	{
		[TestMethod]
		public async Task CorrectVersionOfElasticsearchIsRunning()
		{
			var elasticClient = ElasticClientFactory.CreateElasticClient();

			var response = await elasticClient.RootNodeInfoAsync();
			var actual = response.IsValid;

			Assert.IsTrue(actual, $"Couldn't connect to elasticsearch {response.ServerError}");

			var version = response.Version.Number;

			Assert.AreEqual("6.2.4", version);
		}
	}
}
