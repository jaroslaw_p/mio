﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MIOProject;
using MIOProject.Model;
using Nest;
using System.Linq;
using System.Threading.Tasks;

namespace MIOProjectTest
{
	[TestClass]
	public class CoffeeSearcheTest
	{
		private readonly GeoLocation OurLocation = new GeoLocation(50.262950, 19.013930);
		private readonly string NearMe = "Classica. Restauracja & Kawiarnia";

		[ClassInitialize]
		public static void ClassInitialize(TestContext context)
		{
			var elasticClient = ElasticClientFactory.CreateCustomElasticClient(IndexNames.MioProject);
			elasticClient.DeleteIndex(IndexNames.MioProject);

			var indexer = new CoffeeLocationIndexer(elasticClient);

			var loader = new LocationLoader();
			var loadedLocations = loader.LoadLocation();

			indexer.Index(loadedLocations);
			elasticClient.Refresh(Nest.Indices.Parse(IndexNames.MioProject));
		}

		[TestMethod]
		public void LoadsGoogleLocationsFromFiles()
		{
			var loader = new LocationLoader();
			var loadedLocations = loader.LoadLocation();

			Assert.IsTrue(loadedLocations.Any(), "Didn't load google locations from files properly");
		}

		[TestMethod]
		public async Task DataHasBeenIndexedIntoElasticSearch()
		{
			var elasticClient = ElasticClientFactory.CreateCustomElasticClient(IndexNames.MioProject);
			var response = await elasticClient.CountAsync<CoffeeLocation>();

			Assert.IsTrue(response.Count > 0, $"Not all of documents have been indexed.");
		}

		[TestMethod]
		public async Task FindCoffeeNearMe()
		{
			var actual = await new CoffeeSearcher()
				.NearMe(OurLocation.Latitude, OurLocation.Longitude);

			Assert.AreEqual(NearMe, actual.Name);
		}
	}
}
