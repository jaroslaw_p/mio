﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIOProject.Model
{
	public class CoffeeLocation
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public GeoLocation Location { get; set; }
	}
}
