﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MIOProject;
using System;
using System.IO;
using System.Linq;

namespace MIOProjectTest
{
	[TestClass]
	public class ConsumptionLoaderTest
	{
		[TestMethod]
		public void ReadsTheDataCorectly()
		{
			var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content", "eksport.zip");
			var loader = new ConsumptionLoader();
			var consumptions = loader.LoadConsumption(filePath)
				.ToList();

			Assert.AreEqual(10, consumptions.Count);
		}
	}
}
